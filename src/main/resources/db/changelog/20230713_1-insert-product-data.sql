--liquibase formatted sql
--changeset microservice_class:20230713

INSERT INTO public.product
(product_id,product_code,product_name,product_description,daily_limit,monthly_limit,created_at,updated_at)
VALUES('a51fea9c-ea7a-4a02-bfd4-0eec3aa55a5a','INTERNAL_TRANSFER','Internal bank transfer','Chuyen tien trong ngan hang',20000000,200000000,current_timestamp,current_timestamp);

INSERT INTO public.product
(product_id,product_code,product_name,product_description,daily_limit,monthly_limit,created_at,updated_at)
VALUES('b799f852-4ce1-4412-9248-9911bca6f32f','EXTERNAL_TRANSFER','External bank transfer','Chuyen tien lien ngan hang',10000000,100000000,current_timestamp,current_timestamp);